define([
	'js/misc/physics'
], function (
	physics
) {
	return {
		type: 'aloneStander',

		work: function () {
			let obj = this.obj;

			if (obj.isBusy())
				return;

			let isAlone = objects.getFilteredCell(o => {
				return (o.x === obj.x && o.y === obj.y);
			}).length === 1;

			if (!isAlone) {
				let pos = null;
				let distance = 1;
				while (!pos) {
					pos = physics.getOpenCellInArea(obj.x - distance, obj.y - distance, obj.x + distance, obj.y + distance, obj.x, obj.y);
					distance++;
				}

				const path = physics.getPath({
					x: obj.x, 
					y: obj.y
				}, {
					x: pos.x, 
					y: pos.y
				});

				path.forEach(p => {
					obj.enqueue({
						type: 'moveTo',
						args: {
							x: p.x,
							y: p.y
						}
					});
				});
			}	
		}	
	};
});
