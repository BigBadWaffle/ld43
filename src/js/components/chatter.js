define([
	'js/rendering/renderer'
], function (
	renderer
) {
	return {
		type: 'chatter',

		cd: 0, cdMax: 150, color: 0xf2f5f5,

		init: function () {
			
		},

		extend: function (bpt) {
			for (let p in bpt) 
				this[p] = bpt[p];
		},

		update: function () {
			let chatSprite = this.obj.chatSprite;
			if (!chatSprite) {
				if (this.list && Math.random() < 0.00025)
					this.buildText();

				return;
			}

			if (this.cd > 0) 
				this.cd--;
			else if (this.cd === 0) {
				renderer.destroyObject({
					sprite: chatSprite
				});
				this.obj.chatSprite = null;
			}
		},

		buildText: function (text) {
			const msg = ((text || this.list[~~(Math.random() * this.list.length)]) + '|')
				.split('|')
				.join('\r\n') + '|';

			this.msg = msg;

			let obj = this.obj;

			if (obj.chatSprite) {
				renderer.destroyObject({
					sprite: obj.chatSprite
				});
			}

			if (obj.sprite && !obj.sprite.visible)
				return;

			let color = this.color;
			if (msg[0] === '*')
				color = 0xffeb38;

			let yOffset = (msg.split('\r\n').length - 1);

			obj.chatSprite = renderer.buildText({
				layerName: 'effects',
				text: msg,
				color: color,
				alpha: 0.75,
				x: (obj.x * scale) + (scale / 2),
				y: (obj.y * scale) - (scale * 0.8) - (yOffset * scale * 0.8) - (scaleMult * 3)
			});
			obj.chatSprite.visible = true;

			this.cd = this.cdMax;
		},

		destroy: function () {
			let chatSprite = this.obj.chatSprite;
			if (!chatSprite)
				return;

			renderer.destroyObject({
				sprite: chatSprite
			});
		},

		setVisible: function (visible) {
			let chatSprite = this.obj.chatSprite;
			if (!chatSprite)
				return;

			chatSprite.visible = visible;
		}
	};
});
