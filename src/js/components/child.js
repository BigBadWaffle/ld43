define([
	'js/rendering/renderer',
	'js/system/events',
	'js/misc/nameGenerator'
], function (
	renderer,
	events,
	nameGenerator
) {
	return {
		type: 'child',

		age: 0,
		gender: null,

		genderCell: {
			mother: [1, 3, 6],
			father: [2, 4, 5]
		},

		init: function () {
			
		},

		work: function () {
			this.age++;

			if (this.age === 200) {
				this.growUp();
				this.destroyed = true;
			}
		},

		growUp: function () {
			this.obj.cell = this.genderCell[this.gender][~~(Math.random() * this.genderCell[this.gender].length)];
			renderer.setSprite(this.obj);

			this.obj.ai.reprogram(this.gender);

			this.obj.name = nameGenerator.generate((this.gender === 'father') ? 'male' : 'female');

			if (this.gender === 'father')
				events.emit('onFatherCreated', this.obj);
		}
	};
});
