define([
	'js/system/events'
], function (
	events
) {
	return {
		type: 'pregnant',

		due: 9,

		init: function () {

		},

		work: function () {
			this.due--;

			if (!this.due) {
				this.giveBirth();
				this.destroyed = true;
			}
		},

		giveBirth: function () {
			let gender = ['mother', 'father'][~~(Math.random() * 2)];
			let babyBpt = {
				name: 'Baby',
				sheetName: 'mobs',
				cell: (gender === 'father') ? 13 : 14,
				x: this.obj.x,
				y: this.obj.y,
				components: [{
					type: 'actor'
				}, {
					type: 'ai',
					aiType: 'baby'
				}, {
					type: 'aloneStander'
				}, {
					type: 'child',
					gender: gender
				}]
			};

			babyBpt.id = objects.getNextId();

			events.emit('onGetObject', babyBpt);

			let obj = objects.objects.find(o => o.id === babyBpt.id);
			obj.ai.needs.find(n => n.type === 'eat').value = globals.childInitialHunger;
		}
	};
});
