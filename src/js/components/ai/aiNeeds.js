define([
	
], function (
	
) {
	return {
		defend: {
			translation: 'Defending',
			actions: [{
				type: 'moveTo',
				recalc: true,
				needs: 'eatHuman'
			}, {
				type: 'attack'
			}, {
				type: 'fulfillNeed',
				need: 'defend'
			}]
		},

		makeBabies: {
			translation: 'Making babies',
			increase: 0.025,
			actions: [{
				type: 'addComponent',
				cpn: 'pregnant'
			}, {
				type: 'fulfillNeed',
				need: 'makeBabies'
			}, {
				type: 'wait',
				duration: 15
			}]
		},

		eatHuman: {
			translation: 'Eating a human',
			actions: [{
				type: 'warn'
			}, {
				type: 'moveTo',
				recalc: true,
				needs: 'eat'
			}, {
				type: 'attack'
			}]
		},

		die: {
			translation: 'Dying',
			increase: 100,
			actions: [{
				type: 'chat',
				msg: 'Goodbye, cruel world.'
			}, {
				type: 'wait',
				duration: 10
			}, {
				type: 'destroy'
			}]
		},

		cry: {
			translation: 'Crying',
			increase: 0.1,
			actions: [{
				type: 'chat',
				msg: 'Waah!'
			}, {
				type: 'fulfillNeed',
				need: 'cry'
			}]
		},

		careChildren: {
			translation: 'Caring for my children'
		},

		careHusband: {
			translation: 'Caring for my husband'
		},

		careWife: {
			translation: 'Caring for my wife'
		},

		eat: {
			translation: 'Eating',
			increase: 0.1,
			actions: [{
				type: 'eat'
			}, {
				type: 'fulfillNeed',
				need: 'eat'
			}, {
				type: 'wait',
				duration: 3
			}]
		},

		drink: {
			translation: 'Drinking',
			increase: 0.1,
			actions: [{
				type: 'drink'
			}, {
				type: 'fulfillNeed',
				need: 'drink'
			}, {
				type: 'wait',
				duration: 3
			}]
		},

		sleep: {
			translation: 'Sleeping',
			increase: 0.05,
			actions: [{
				type: 'chat',
				msg: '*yawns*'
			}, {
				type: 'moveTo',
				location: 'home'
			}, {
				type: 'wait',
				duration: 5
			}, {
				type: 'chat',
				msg: '*zzz*'
			}, {
				type: 'wait',
				duration: 20
			}, {
				type: 'fulfillNeed',
				need: 'sleep'
			}]
		},

		clean: {
			translation: 'Cleaning',
			increase: 0.005
		},

		getWood: {
			translation: 'Obtaining wood',
			default: true,
			actions: [{
				type: 'moveTo',
				layerCheck: 'walls',
				cellCheck: [0],
				distance: 1
			}, {
				type: 'wait',
				duration: 10
			}, {
				type: 'moveTo',
				location: 'home'
			}, {
				type: 'getWood',
				amount: 1
			}]
		},

		getWater: {
			translation: 'Obtaining water',
			default: true,
			actions: [{
				type: 'moveTo',
				pos: {
					x: 41,
					y: 26
				}
			}, {
				type: 'wait',
				duration: 10
			}, {
				type: 'moveTo',
				location: 'home'
			}, {
				type: 'getWater',
				amount: globals.waterPerHaul
			}]
		},

		beVendor: {
			translation: 'Being a vendor',
			actions: [{
				type: 'wait',
				duration: globals.vendorDelay
			}, {
				type: 'appear'
			}, {
				type: 'moveTo',
				pos: {
					x: 24,
					y: 11
				}
			}, {
				type: 'chat',
				msg: 'Might I interest you|in my wares?'
			}, {
				type: 'event',
				event: 'onVendorArrives'
			}, {
				type: 'wait',
				duration: globals.vendorStay
			}, {
				type: 'event',
				event: 'onVendorLeaves'
			}, {
				type: 'chat',
				msg: 'Until next time.'
			}, {
				type: 'moveTo',
				pos: {
					x: 17,
					y: 16
				}
			}, {
				type: 'moveTo',
				pos: {
					x: 0,
					y: 16
				}
			}, {
				type: 'vanish'
			}, {
				type: 'moveTo',
				pos: {
					x: 31,
					y: 0
				}
			}]
		},

		beBuilder: {
			translation: 'Being a builder',
			actions: [{
				type: 'wait',
				duration: globals.builderDelay
			}, {
				type: 'appear'
			}, {
				type: 'moveTo',
				pos: {
					x: 29,
					y: 14
				}
			}, {
				type: 'chat',
				msg: 'Need anything built?'
			}, {
				type: 'event',
				event: 'onBuilderArrives'
			}, {
				type: 'wait',
				duration: globals.builderStay
			}, {
				type: 'event',
				event: 'onBuilderLeaves'
			}, {
				type: 'chat',
				msg: 'Farewell.'
			}, {
				type: 'moveTo',
				pos: {
					x: 17,
					y: 16
				}
			}, {
				type: 'moveTo',
				pos: {
					x: 0,
					y: 16
				}
			}, {
				type: 'vanish'
			}, {
				type: 'moveTo',
				pos: {
					x: 31,
					y: 0
				}
			}]
		},

		beGeneral: {
			translation: 'Being a general',
			actions: [{
				type: 'wait',
				duration: globals.generalDelay
			}, {
				type: 'appear'
			}, {
				type: 'moveTo',
				pos: {
					x: 17,
					y: 17
				}
			}, {
				type: 'chat',
				msg: 'The war rages and|your country needs you.'
			}, {
				type: 'event',
				event: 'onGeneralArrives'
			}, {
				type: 'waitEvent',
				event: 'onDispatchMan'
			}, {
				type: 'event',
				event: 'onGeneralLeaves'
			}, {
				type: 'chat',
				msg: 'Your country is counting|on you sir.'
			}, {
				type: 'moveTo',
				pos: {
					x: 31,
					y: 0
				}
			}, {
				type: 'vanish'
			}, {
				type: 'moveTo',
				pos: {
					x: 0,
					y: 17
				}
			}, {
				type: 'wait',
				duration: globals.generalPostDelay
			}]
		},

		beAnimal: {
			translation: 'Being an animal',
			actions: [{
				type: 'wait',
				duration: globals.foodDelay
			}, {
				type: 'getFood',
				amount: 1
			}]
		},

		goToWar: {
			translation: 'Going to the war',
			actions: [{
				type: 'chat',
				msg: 'Duty calls!'
			}, {
				type: 'moveTo',
				pos: {
					x: 0,
					y: 17
				}
			}, {
				type: 'destroy'
			}]
		}
	};
});
