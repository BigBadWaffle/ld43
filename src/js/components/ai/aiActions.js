define([
	'js/misc/physics',
	'js/system/events',
	'js/map/map'
], function (
	physics,
	events,
	map
) {
	return {
		addComponent: function (config) {
			this.obj.addComponent(config.cpn, {});
			this.completeAction();
		},

		fulfillNeed: function (config) {
			let need = this.needs.find(n => n.type === config.need);
			need.value = Math.max(need.value - 5, 0);
			this.completeAction();
		},

		warn: function () {
			objects.objects.filter(o => o.ai && o.ai.aiType === 'father')
				.forEach(o => {
					o.ai.needs.find(n => n.type === 'defend').value = 999;
					o.ai.actionIndex = 0;
					o.ai.currentActions = null;
				});

			this.completeAction();
		},

		attack: function () {
			let obj = this.obj;
			let x = obj.x;
			let y = obj.y;

			let targetType = this.aiType === 'father' ? 'eatHuman' : 'eat';
			let target = physics.getArea(x - 1, y - 1, x + 1, y + 1, c => c.ai && c.ai.needs.some(n => n.type === targetType))[0];
			if (!target) {
				this.actionIndex--;
				return;
			}

			if (!target.hp) {
				if (this.aiType === 'wolf')
					target.hp = 10;
				else
					target.hp = 4;
			}

			target.hp--;

			if (Math.random() < 0.1) {
				if (target.ai.aiType === 'father')
					target.chatter.buildText('Foul beast!');
				else if (target.aiType === 'mother')
					target.chatter.buildText('Help!');
			}

			if (!target.hp) {
				target.clearQueue();
				target.ai.reprogram('dying');

				if (this.aiType === 'father') {
					let moreTargets = objects.getFilteredCell(o => o.ai && o.ai.needs.some(n => n.type === 'eatHuman'));
					if (!moreTargets.length) {
						this.completeAction();
						this.needs.find(n => n.type === 'defend').value = 0;
					}
				}
			}
		},

		moveTo: function (config) {
			const obj = this.obj;

			if (config.recalc)
				this.obj.clearQueue();

			if (this.obj.isBusy())
				return;
			else if (this.target) {
				let distance = Math.max(Math.abs(obj.x - this.target.x), Math.abs(obj.y - this.target.y));

				if (distance <= 1) {
					this.target = null;
					this.completeAction();
					return;
				}
			}

			let target = null;
			if (config.name)
				target = objects.find(config.name);
			else if (config.location === 'home')
				target = obj.spawnPos;
			else if (config.pos)
				target = config.pos;
			else if (config.needs) {
				let options = objects.getFilteredCell(o => o.ai && o.ai.needs.some(n => n.type === config.needs));
				if (!options.length)
					target = null;
				else
					target = options[~~(Math.random() * options.length)];
			} else if (config.layerCheck)
				target = map.getCellWith(config.layerCheck, config.cellCheck);

			this.target = target;

			if (!target)
				return;

			distance = Math.max(Math.abs(obj.x - target.x), Math.abs(obj.y - target.y));

			if (distance <= 1) {
				this.target = null;
				this.completeAction();
				return;
			}

			const path = physics.getPath({
				x: obj.x, 
				y: obj.y
			}, {
				x: target.x, 
				y: target.y
			}, config.distance || 0);

			path.forEach(p => {
				obj.enqueue({
					type: 'moveTo',
					args: {
						x: p.x,
						y: p.y
					}
				});
			});
		},

		getFood: function (config) {
			let amount = config.amount;
			if (this.obj.ai && this.obj.ai.aiType === 'animal')
				amount *= this.obj.ai.foodQuantity;

			events.emit('onGetDamage', {
				event: true,
				text: '+food',
				id: this.obj.id
			});
			events.emit('onGetResource', 'food', amount);
			this.completeAction();
		},

		getWood: function (config) {
			events.emit('onGetDamage', {
				event: true,
				text: '+wood',
				id: this.obj.id
			});

			events.emit('onGetResource', 'wood', config.amount);
			this.completeAction();
		},

		getWater: function (config) {
			events.emit('onGetDamage', {
				event: true,
				text: '+water',
				id: this.obj.id
			});

			events.emit('onGetResource', 'water', config.amount);
			this.completeAction();
		},

		chat: function (config) {
			if (!this.isAnimal)
				this.obj.chatter.buildText(config.msg);
			this.completeAction();
		},

		eat: function (config) {
			let hud = $('.uiHud').data('ui');
			if (hud.resources.food < 1)
				return;

			events.emit('onGetResource', 'food', -1);
			this.completeAction();
		},

		drink: function (config) {
			let hud = $('.uiHud').data('ui');
			if (hud.resources.water < 1) {
				if (this.needs.some(n => n.type === 'getWater'))
					this.startNeed('getWater');

				return;
			}

			events.emit('onGetResource', 'water', -1);
			this.completeAction();
		},

		checkResource: function (config) {
			let hud = $('.uiHud').data('ui');
			if (hud.resources[config.resource] < config.min) {
				if (this.needs.some(n => n.type === config.ifNotState))
					this.startNeed(config.ifNotState);
				else if (config.ifNotMsg)
					this.obj.chatter.buildText(config.ifNotMsg);
			} else
				this.completeAction();
		},

		wait: function (config) {
			config.duration--;
			if (config.duration === 0)
				this.completeAction();
		},

		waitEvent: function (config) {
			if (config.listener)
				return;

			const listener = () => {
				events.off(config.event, config.listener);
				this.completeAction();
			};

			config.listener = events.on(config.event, listener.bind(this));
		},

		event: function (config) {
			events.emit(config.event, this.obj);
			this.completeAction();
		},

		vanish: function (config) {
			this.obj.setVisible(false);
			this.completeAction();
		},

		appear: function (config) {
			this.obj.setVisible(true);
			this.completeAction();
		},

		destroy: function (config) {
			this.obj.destroyed = true;
		}
	};
});
