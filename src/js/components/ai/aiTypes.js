define([
	
], function (
	
) {
	return {
		default: {

		},

		wolf: {
			needs: {
				eatHuman: 10
			}
		},

		baby: {
			needs: {
				eat: 1,
				drink: 0.7,
				sleep: 0.7,
				cry: 0.4
			}
		},

		dying: {
			needs: {
				die: 100
			}
		},

		mother: {
			needs: {
				getWater: 0.3,
				makeBabies: globals.needMotherMakeBabies,
				drink: 0.3,
				eat: 0.5,
				clean: 0.45	
			}
		},

		father: {
			needs: {
				getWood: 1,
				eat: 0.6,
				drink: 0.6,
				sleep: 0.6,
				defend: 0
			}
		},

		vendor: {
			needs: {
				beVendor: 10
			}
		},

		builder: {
			needs: {
				beBuilder: 10
			}
		},

		general: {
			needs: {
				beGeneral: 10
			}
		},

		animal: {
			needs: {
				beAnimal: 10
			}
		},

		soldier: {
			needs: {
				goToWar: 10
			}
		}
	};
});
