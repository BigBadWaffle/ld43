define([
	
], function (
	
) {
	return {
		type: 'actor',

		init: function () {
			this.obj.addComponent('chatter');
		},

		work: function () {
			let ai = this.obj.ai;
			if (!ai)
				return;

			let died = false;
			['eat', 'drink'].forEach(i => {
				if (died)
					return;
				
				let need = ai.needs.find(n => n.type === i);
				if (!need)
					return;

				if (need.value >= 15) {
					died = true;
					ai.reprogram('dying');
				} else if (need.value >= 10) {
					let msg = {
						eat: 'So hungry...',
						drink: 'So thirsty...'
					}[i];
					this.obj.chatter.buildText(msg);
				}
			});
		}
	};
});
