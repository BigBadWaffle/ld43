let components = [
	'actor',
	'ai',
	'aloneStander',
	'chatter',
	'child',
	'general',
	'pregnant'
].map(function (c) {
	return 'js/components/' + c;
});

define(components, function () {
	let templates = {};

	[].forEach.call(arguments, function (t) {
		templates[t.type] = t;
	});

	return {
		getTemplate: function (type) {
			let template = templates[type] || {
				type: type
			};

			return template;
		}
	};
});
