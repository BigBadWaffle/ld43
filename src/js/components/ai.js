define([
	'js/components/ai/aiTypes',
	'js/components/ai/aiNeeds',
	'js/components/ai/aiActions'
], function (
	aiTypes,
	aiNeeds,
	aiActions
) {
	return {
		type: 'ai',

		aiType: 'default',
		needs: null,

		currentActions: null,
		actionIndex: 0,
		busy: false,

		init: function (bpt) {
			this.needs = Object.entries(aiTypes[this.aiType].needs)
				.map(([k, v]) => {
					return {
						type: k,
						name: aiNeeds[k].translation,
						value: v,
						increase: v * aiNeeds[k].increase
					};
				});
		},

		reprogram: function (newAiType) {
			if (this.aiType === newAiType)
				return;
			
			this.currentActions = null;
			this.busy = false;
			this.actionIndex = 0;
			this.aiType = newAiType;
			this.init();
		},

		startNeed: function (newNeed) {
			let actions = aiNeeds[newNeed].actions;
			if (actions)
				this.currentActions = $.extend(true, [], actions);
			this.actionIndex = 0;
		},

		work: function () {
			this.needs.forEach(n => {
				if (!n.increase)
					return;
				
				n.value += n.increase;
			});

			if (this.currentActions && this.currentActions.length) {
				this.performActions();
				return;
			}

			const highestNeed = this.getHighestNeed();
			if (!highestNeed)
				return;

			let actions = aiNeeds[highestNeed.type].actions;
			if (actions)
				this.currentActions = $.extend(true, [], actions);
			this.actionIndex = 0;
		},

		getHighestNeed: function () {
			this.needs.sort((a, b) => {
				return b.value - a.value;
			});

			const highest = this.needs[0];
			if (highest.value >= 5)
				return highest;

			let defaultNeed = Object.entries(aiNeeds).find(([n, v]) => v.default && this.needs.some(f => f.type === n));
			if (defaultNeed)
				return this.needs.find(n => n.type === defaultNeed[0]);
		},

		performActions: function () {
			const currentAction = this.currentActions[this.actionIndex];
			aiActions[currentAction.type].call(this, currentAction);
		},

		completeAction: function () {
			this.actionIndex++;
			if (this.actionIndex >= this.currentActions.length)
				this.currentActions = null;
		}
	};
});
