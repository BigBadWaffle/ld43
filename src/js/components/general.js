define([
	'js/system/events'
], function (
	events
) {
	return {
		type: 'general',

		wait: 0,
		active: false,

		init: function () {
			
		},

		activate: function () {
			this.active = true;
			this.wait = 100;
		},

		work: function () {
			if (!this.active)
				return;

			this.wait--;

			if (!this.wait) {
				this.takeRandomMan();
				this.takeResources();
				this.active = false;
				this.obj.ai.completeAction();
			} else if (this.wait === 25)
				this.obj.chatter.buildText('You dawdle. You will|pay for ignoring me!');
		},

		takeRandomMan: function () {
			let men = objects.getFilteredCell(o => o.ai && o.ai.aiType === 'father');
			if (!men.length)
				return;

			let man = men[~~(Math.random() * men.length)];

			man.clearQueue();
			man.ai.reprogram('soldier');
		},

		takeResources: function () {
			let resources = $('.uiHud').data('ui').resources;
			let pick = Object.keys(resources).sort((a, b) => {
				return resources[b] - resources[a];
			})[0];

			events.emit('onGetResource', pick, -Math.min(resources[pick], ~~(resources[pick] / 2)));
		}
	};
});
