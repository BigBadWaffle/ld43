define([
	'config/namesMale',
	'config/namesFemale'
], function (
	namesMale,
	namesFemale
) {
	return {
		trees: {
			male: {},
			female: {}
		},

		init: function () {
			this.build(namesMale, this.trees.male);
			this.build(namesFemale, this.trees.female);
		},

		generate: function (type) {
			let tree = this.trees[type];
			let bucket = tree;

			let name = '';

			while (true) {
				let options = [];
				Object.entries(bucket).forEach(([k, v]) => {
					for (let i = 0; i < v.weight; i++) 
						options.push(k);
				});

				let pick = options[~~(Math.random() * options.length)];
				if (pick === '.')
					break;

				bucket = tree[pick];
				name += pick;
			}

			if (name.length < 3 || name.length > 8)
				return this.generate(type);

			return name;
		},

		build: function (list, target) {
			list.forEach(n => this.iterate(n, target));
		},

		iterate: function (word, target, prev) {
			let bucket = target;
			if (prev) {
				bucket = target[prev];

				if (!bucket) 
					bucket = target[prev] = {};
			}

			let letter = word[0] || '.';
			if (!bucket[letter]) {
				bucket[letter] = {
					weight: 0
				};
			}

			bucket[letter].weight++;

			if (letter === '.')
				return;

			this.iterate(word.substr(1), target, letter);
		}
	};
});
