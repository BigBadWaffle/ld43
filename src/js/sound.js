define([
	'howler',
	'js/misc/physics',
	'js/system/events'
], function (
	howler,
	physics,
	events
) {
	return {
		sounds: [],

		muted: false,

		init: function () {
			this.addSound('song', 'song1.ogg', 0.5);
			this.addSound('talk', 'talk.ogg', 1);
			this.addSound('tick', 'tick.ogg', 0.5);
			this.play('song', true);

			events.on('onKeyDown', this.onKeyDown.bind(this));
		},

		onKeyDown: function (key) {
			if (key === 'm')
				this.toggleMute();
		},

		play: function (name, loop) {
			let s = this.sounds.find(f => f.name === name);
			if (!s.sound) {
				s.sound = new Howl({
					src: ['audio/' + s.file],
					autoplay: true,
					loop: loop,
					volume: s.volume,
					muted: this.muted
				});
			} else
				s.sound.play();
		},

		addSound: function (name, file, volume) {
			let sound = {
				name: name,
				file: file,
				volume: volume,
				sound: null
			};

			this.sounds.push(sound);
		},

		toggleMute: function () {
			const muted = this.muted = !this.muted;

			this.sounds.forEach(s => {
				if (!s.sound)
					return;

				s.sound.mute(muted);
			});
		}
	};
});
