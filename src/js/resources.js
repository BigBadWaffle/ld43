define([
	'js/system/events'
], function (
	events
) {
	return {
		spriteNames: [
			'tiles',
			'walls',
			'mobs'
		],

		sprites: {},
		ready: false,

		init: function (list) {
			this.spriteNames.forEach(function (s) {
				let sprite = {
					image: (new Image()),
					ready: false
				};
				sprite.image.src = 'images/' + s + '.png';
				sprite.image.onload = this.onSprite.bind(this, sprite);

				this.sprites[s] = sprite;
			}, this);
		},

		onSprite: function (sprite) {
			sprite.ready = true;

			let readyCount = 0;
			for (let s in this.sprites) {
				if (this.sprites[s].ready)
					readyCount++;
			}

			if (readyCount === this.spriteNames.length)
				this.onReady();
		},

		onReady: function () {
			this.ready = true;

			events.emit('onResourcesLoaded');
		}
	};
});
