define([
	'js/misc/physics',
	'js/misc/nameGenerator',
	'js/system/events'
], function (
	physics,
	nameGenerator,
	events
) {
	return {
		w: 0,
		h: 0,
		layers: null,
		tilesets: null,

		resolver: null,
		loaded: null,

		wolfCounter: 0,

		init: async function (number = 0) {
			return new Promise(res => {
				this.resolver = res;

				this.loadMap(number);
			});			
		},

		work: function () {
			if (this.wolfCounter++ === globals.wolfCd) {
				this.spawnWolf();
				globals.wolfCd = Math.max(globals.wolfCdMin, globals.wolfCd * globals.wolfCdMult);
			}
		},

		spawnWolf: function () {
			let wolfBpt = {
				name: 'Wolf',
				sheetName: 'mobs',
				cell: 9,
				x: 3,
				y: 3,
				components: [{
					type: 'actor'
				}, {
					type: 'ai',
					aiType: 'wolf'
				} ]
			};

			wolfBpt.id = objects.getNextId();
			events.emit('onGetObject', wolfBpt);

			objects.objects.find(o => o.id === wolfBpt.id).chatter.buildText('*snarls*');
		},

		getCellWith: function (layerName, list) {
			let layerData = this.layers.find(l => l.name === layerName).data;

			let tries = 10;
			while (tries--) {
				let x = ~~(Math.random() * this.w);
				let y = ~~(Math.random() * this.h);

				if (list.includes(layerData[x][y])) {
					return {
						x,
						y
					};
				}
			}
		},

		getUpgradeCost: function () {
			return ((this.loaded + 1) * globals.townUpgradeMultiplier);
		},

		upgrade: async function () {
			await this.init(this.loaded + 1);
			this.layers.find(l => l.name === 'mobs').objects.forEach(m => {
				let bpt = $.extend(true, {}, m);
				bpt.id = objects.getNextId();

				let obj = objects.buildObject(bpt);
				if (!bpt.visible)
					obj.setVisible(false);
			});
		},

		loadMap: function (number) {
			this.loaded = number;
			require(['json!maps/town' + number + '.json'], this.onLoadMap.bind(this));
		},

		onLoadMap: function (map) {
			this.w = map.width;
			this.h = map.height;

			let collisionMap = _.get2dArray(this.w, this.h, 0);

			this.tilesets = map.tilesets
				.map(t => {
					return {
						name: t.name,
						first: t.firstgid,
						count: t.tilecount
					};
				});

			this.layers = map.layers
				.map(l => {
					let res = {
						name: l.name
					};

					if (l.data) {
						res.data = _.get2dArray(this.w, this.h);

						l.data.forEach((d, i) => {
							const y = ~~(i / this.w);
							const x = i - (y * this.w);

							let mapped = d ? this.mapTile(d) : null;
							res.data[x][y] = mapped;

							if (mapped !== null && l.name === 'walls')
								collisionMap[x][y] = 1;
						});
					} else if (l.objects) {
						res.objects = l.objects
							.map(r => {
								let res = {
									name: r.name,
									sheetName: l.name,
									x: r.x / 8,
									y: (r.y / 8) - 1,
									visible: r.visible,
									cell: this.mapTile(r.gid),
									components: Object.entries(r.properties || {})
										.map(([ k, v ]) => {
											const config = JSON.parse(v);
											return {
												type: k
													.replace('cpn', '')
													.replace(/./, k[3].toLowerCase()),
												...config
											};
										})
								};

								let ai = res.components.find(c => c.type === 'ai');
								if (ai) {
									let aiType = ai.aiType;
									if (aiType === 'father')
										res.name = nameGenerator.generate('male');
									else if (aiType === 'mother')
										res.name = nameGenerator.generate('female');
								}

								return res;
							});
					}

					return res;
				});

			physics.init(collisionMap);

			this.resolver();
		},

		mapTile: function (tile) {
			const set = this.tilesets.find(t => {
				return (tile < t.first + t.count);
			});

			return tile - set.first;
		}
	};
});
