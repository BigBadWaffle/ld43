define([
	'ui/factory',
	'js/rendering/renderer',
	'js/objects/objects',
	'js/input',
	'js/system/events',
	'js/resources',
	'js/map/map',
	'js/sound',
	'js/misc/nameGenerator',
	'js/rendering/numbers'
], function (
	uiFactory,
	renderer,
	objects,
	input,
	events,
	resources,
	map,
	sound,
	nameGenerator,
	numbers
) {
	window.objects = objects;

	return {
		hasFocus: true,

		init: async function () {
			window.scale = Math.min(24, $('body').height() / 35);
			window.scaleMult = window.scale / 8;

			let cc = $('.canvas-container');
			cc.width(scale * 50);
			cc.height(scale * 35);

			let uc = $('.ui-container');
			uc.width(scale * 50);
			uc.height(scale * 35);

			nameGenerator.init();
			await map.init();
			resources.init();
			sound.init();
			numbers.init();

			events.on('onResourcesLoaded', this.start.bind(this));
		},

		start: async function () {
			window.onfocus = this.onFocus.bind(this, true);
			window.onblur = this.onFocus.bind(this, false);

			$(window).on('contextmenu', this.onContextMenu.bind(this));

			objects.init();
			renderer.init();
			input.init();
			uiFactory.init();

			$('.canvas-container').hide();
			$('.loader-container').remove();

			let uiMenu = await uiFactory.build('menu');
			await uiMenu.waitForPlay();
			uiFactory.destroy('menu');

			this.startGame();

			$('.canvas-container').show();

			this.update();
			this.work();
		},

		startGame: function () {
			events.emit('onEnterGame');

			renderer.buildMap();
			objects.buildMap();
		},

		onFocus: function (hasFocus) {
			//Hack: Later we might want to make it not render when out of focus
			this.hasFocus = true;

			if (!hasFocus)
				input.resetKeys();
		},

		onContextMenu: function (e) {
			e.preventDefault();
			return false;
		},

		update: function () {
			objects.update();
			renderer.update();
			uiFactory.update();

			renderer.render();
			numbers.render();

			requestAnimationFrame(this.update.bind(this));
		},

		work: async function () {
			objects.work();
			map.work();

			if (!objects.getFilteredCell(o => o.ai && ['mother', 'father', 'baby', 'dying'].includes(o.ai.aiType)).length) {
				uiFactory.destroy();

				let uiLoss = await uiFactory.build('loss');
				await uiLoss.waitForRetry();

				objects.objects = [];
				renderer.clean();

				map.loaded = null;
				await map.init();

				this.startGame();
			} else if (map.loaded === 3) {
				uiFactory.destroy();

				let uiVictory = await uiFactory.build('victory');
				await uiVictory.waitForRetry();

				objects.objects = [];
				renderer.clean();

				map.loaded = null;
				await map.init();

				this.startGame();
			}

			setTimeout(this.work.bind(this), 100);
		}
	};
});
