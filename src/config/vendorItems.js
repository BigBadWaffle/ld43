define([
	
], function (
	
) {
	return {
		food: {
			type: 'resource',
			price: 2,
			townLevel: 0
		},
		water: {
			type: 'resource',
			price: 1,
			townLevel: 0
		},
		wood: {
			type: 'resource',
			price: 3,
			townLevel: 0
		},
		chicken: {
			type: 'obj',
			price: 8,
			townLevel: 0,
			bpt: {
				name: 'Chicken',
				sheetName: 'mobs',
				cell: 8,
				x: 11,
				y: 25,
				components: [{
					type: 'actor'
				}, {
					type: 'ai',
					aiType: 'animal',
					foodQuantity: globals.foodChicken
				}, {
					type: 'aloneStander'
				}]
			}
		},
		cow: {
			type: 'obj',
			price: 15,
			townLevel: 1,
			bpt: {
				name: 'Cow',
				sheetName: 'mobs',
				cell: 10,
				x: 37,
				y: 14,
				components: [{
					type: 'actor'
				}, {
					type: 'ai',
					aiType: 'animal',
					foodQuantity: globals.foodCow
				}, {
					type: 'aloneStander'
				}]
			}
		},
		pig: {
			type: 'obj',
			price: 27,
			townLevel: 2,
			bpt: {
				name: 'Pig',
				sheetName: 'mobs',
				cell: 11,
				x: 37,
				y: 14,
				components: [{
					type: 'actor'
				}, {
					type: 'ai',
					aiType: 'animal',
					foodQuantity: globals.foodPig
				}, {
					type: 'aloneStander'
				}]
			}
		}
	};
});
