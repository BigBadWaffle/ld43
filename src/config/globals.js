define([
	
], function (
	
) {
	window.globals = {
		initialCoins: 0,
		initialFood: 10,
		initialWood: 0,
		initialWater: 10,

		vendorDelay: 150,
		vendorStay: 100,

		builderDelay: 500,
		builderStay: 100,

		townUpgradeMultiplier: 10,

		foodChicken: 1,
		foodCow: 2.1,
		foodPig: 3.2,

		childInitialHunger: 2,

		generalDelay: 1000,
		generalPostDelay: 600,

		foodDelay: 50,

		wolfCd: 500,
		wolfCdMult: 0.95,
		wolfCdMin: 100,

		waterPerHaul: 1.5,

		needMotherMakeBabies: 0.45
	};
});
