define([
	'js/system/events',
	'html!ui/templates/general/template',
	'css!ui/templates/general/styles',
	'html!ui/templates/general/tplItem',
	'js/sound'
], function (
	events,
	template,
	styles,
	tplItem,
	sound
) {
	return {
		tpl: template,

		postRender: function () {
			this.onEvent('onGeneralArrives', this.events.onGeneralArrives.bind(this));
			this.onEvent('onGeneralLeaves', this.events.onGeneralLeaves.bind(this));
			this.onEvent('onObjectDestroyed', this.build.bind(this));
			this.onEvent('onFatherCreated', this.build.bind(this));

			this.build();
		},

		build: function () {
			let container = this.find('.list').empty();

			let men = objects.getFilteredCell(o => o.ai && o.ai.aiType === 'father');

			men
				.forEach(m => {
					let html = tplItem
						.split('|name').join(m.name);

					$(html)
						.appendTo(container)
						.on('click', this.sendMan.bind(this, m));
				});
		},

		sendMan: function (man) {
			sound.play('tick');
			man.clearQueue();
			man.ai.reprogram('soldier');
			events.emit('onDispatchMan', man);
		},

		events: {
			onGeneralArrives: function (obj) {
				obj.general.activate();
				sound.play('talk');
				this.build();
				this.show();	
			},

			onGeneralLeaves: function (obj) {
				sound.play('talk');
				this.hide();
			}
		}
	};
});
