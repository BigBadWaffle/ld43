define([
	'js/system/events',
	'html!ui/templates/menu/template',
	'css!ui/templates/menu/styles',
	'js/sound'
], function (
	events,
	template,
	styles,
	sound
) {
	return {
		tpl: template,

		resolver: null,

		postRender: function () {
			this.find('.btnPlay').on('click', this.events.onPlay.bind(this));
			this.find('.btnMute').on('click', this.events.onMute.bind(this));

			events.on('onKeyDown', this.events.onKeyDown.bind(this));
		},

		waitForPlay: async function () {
			return new Promise(res => {
				this.resolver = res;
			});
		},

		events: {
			onPlay: function () {
				sound.play('tick');
				this.resolver();
			},

			onMute: function () {
				sound.toggleMute();
			},

			onKeyDown: function (key) {
				if (key === 'p')
					this.events.onPlay.call(this);
			}
		}
	};
});
