define([
	'js/system/events',
	'html!ui/templates/hud/template',
	'css!ui/templates/hud/styles'
], function (
	events,
	template,
	styles
) {
	return {
		tpl: template,

		resources: null,

		postRender: function () {
			this.reset();

			this.onEvent('onGetResource', this.events.onGetResource.bind(this));
			this.onEvent('onBeforeBuy', this.events.onBeforeBuy.bind(this));
			this.onEvent('onBeforeSell', this.events.onBeforeSell.bind(this));

			this.build();
		},

		reset: function () {
			this.resources = {
				coins: globals.initialCoins,
				food: globals.initialFood,
				wood: globals.initialWood,
				water: globals.initialWater
			};

			this.build();
		},

		build: function () {
			let resources = this.resources;

			Object.entries(resources).forEach(([k, v]) => {
				this.find(`[type="${k}"] .amount`).html(~~v);
			});
		},

		events: {
			onGetResource: function (type, amount) {
				this.resources[type] += amount;
				events.emit('onUpdateResources');

				this.build();
			},

			onBeforeBuy: function (msg) {
				if (msg.item.buyPrice > this.resources.coins)
					msg.success = false;
			},

			onBeforeSell: function (msg) {
				const item = msg.item;

				if (item.type === 'resource') 
					msg.success = this.resources[item.name] > 0;
			}
		}
	};
});
