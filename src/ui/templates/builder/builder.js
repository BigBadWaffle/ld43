define([
	'js/system/events',
	'html!ui/templates/builder/template',
	'css!ui/templates/builder/styles',
	'js/map/map',
	'js/rendering/renderer',
	'js/sound'
], function (
	events,
	template,
	styles,
	map,
	renderer,
	sound
) {
	return {
		tpl: template,

		builder: null,

		postRender: function () {
			this.onEvent('onBuilderArrives', this.events.onBuilderArrives.bind(this));
			this.onEvent('onBuilderLeaves', this.events.onBuilderLeaves.bind(this));

			this.find('.btnUpgrade').on('click', this.events.onUpgrade.bind(this));
			this.find('.btnDismiss').on('click', this.events.onDismiss.bind(this));
		},

		build: function () {
			let cost = map.getUpgradeCost();
			let msg = `Upgrading your town will cost ${cost} wood`;
			this.find('.msg').html(msg);

			let resources = $('.uiHud').data('ui').resources;

			this.el.removeClass('insufficient-wood');

			if (resources.wood < cost)
				this.el.addClass('insufficient-wood');
		},

		events: {
			onBuilderArrives: function (obj) {
				sound.play('talk');
				this.builder = obj;
				this.build();
				this.show();	
			},

			onBuilderLeaves: function () {
				this.hide();
			},

			onUpgrade: async function () {
				sound.play('tick');
				let cost = map.getUpgradeCost();
				events.emit('onGetResource', 'wood', -cost);

				await map.upgrade();
				renderer.clean(['tiles', 'walls']);
				renderer.buildMap();
				this.builder.ai.completeAction();
			},

			onDismiss: function () {
				sound.play('tick');
				this.builder.ai.completeAction();
			}
		}
	};
});
