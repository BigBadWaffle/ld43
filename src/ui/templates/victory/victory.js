define([
	'js/system/events',
	'html!ui/templates/victory/template',
	'css!ui/templates/victory/styles',
	'js/sound',
	'ui/factory'
], function (
	events,
	template,
	styles,
	sound,
	uiFactory
) {
	return {
		tpl: template,

		resolver: null,

		postRender: function () {
			this.find('.btnRetry').on('click', this.events.onRetry.bind(this));
			events.on('onKeyDown', this.events.onKeyDown.bind(this));
		},

		waitForRetry: async function () {
			return new Promise(res => {
				this.resolver = res;
			});
		},

		events: {
			onRetry: function () {
				sound.play('tick');
				uiFactory.destroy('victory');
				this.resolver();
			},

			onKeyDown: function (key) {
				if (key === 'r')	
					this.events.onRetry.call(this);
			}
		}
	};
});
