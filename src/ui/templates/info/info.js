define([
	'js/system/events',
	'html!ui/templates/info/template',
	'css!ui/templates/info/styles',
	'js/input'
], function (
	events,
	template,
	styles,
	input
) {
	return {
		tpl: template,

		hovered: null,

		postRender: function () {
			this.onEvent('onMobHover', this.events.onMobHover.bind(this));
		},

		events: {
			onMobHover: function (obj) {
				this.hovered = obj;

				if (obj) {
					this.el
						.css({
							left: input.mouseRaw.offsetX + 20,
							top: input.mouseRaw.offsetY
						})
						.find('.name')
						.html(obj.name);

					if (obj.ai) {
						let needs = obj.ai.needs
							.filter(n => n.value > 2)
							.map(n => n.type + ': ' + ~~n.value);
						if (!needs.length)
							needs = ['...is content'];

						needs = needs.join('<br />');

						this.el.find('.needs').html(needs);
					} else
						this.el.find('.needs').html('');

					this.show();
				} else
					this.hide();
			}
		}
	};
});
