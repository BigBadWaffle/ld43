define([
	'js/system/events',
	'html!ui/templates/vendor/template',
	'css!ui/templates/vendor/styles',
	'html!ui/templates/vendor/tplItem',
	'config/vendorItems',
	'js/map/map',
	'js/sound'
], function (
	events,
	template,
	styles,
	tplItem,
	vendorItems,
	map,
	sound
) {
	return {
		tpl: template,

		action: null,
		markup: 1,

		vendor: null,

		postRender: function () {
			this.onEvent('onVendorArrives', this.events.onVendorArrives.bind(this));
			this.onEvent('onVendorLeaves', this.events.onVendorLeaves.bind(this));
			this.onEvent('onGetResource', this.buildShop.bind(this, null));

			this.find('.btnSell').on('click', this.buildShop.bind(this, 'sell'));
			this.find('.btnBuy').on('click', this.buildShop.bind(this, 'buy'));
			this.find('.btnBack').on('click', this.hideShop.bind(this, true));
			this.find('.btnDismiss').on('click', this.events.onDismiss.bind(this));
		},

		hideShop: function (playSound) {
			if (playSound)
				sound.play('tick');
			this.action = null;
			this.el.removeClass('showing-list');
		},

		buildShop: function (type) {
			if (!type && !this.action)
				return;

			type = type || this.action;

			this.action = type;

			this.el.addClass('showing-list');

			let title = `What would you like to ${type}?`;
			this.find('.title').html(title);

			let container = this.find('.list').empty();

			let resources = $('.uiHud').data('ui').resources;

			this.getList(type)
				.forEach(l => {
					let price = type === 'buy' ? l.buyPrice : l.price;

					let html = tplItem
						.split('|name').join(l.name)
						.split('|price').join(price);

					let el = $(html)
						.appendTo(container)
						.on('click', this.buySell.bind(this, type, l));

					if (
						(type === 'buy' && resources.coins < price) ||
						(type === 'sell' && (resources[l.name] < 1))
					)
						el.addClass('disabled');
				});
		},

		buySell: function (type, item) {
			sound.play('tick');
			if (type === 'buy') {
				let msgBeforeBuy = {
					success: true,
					item: item
				};
				events.emit('onBeforeBuy', msgBeforeBuy);
				if (!msgBeforeBuy.success)
					return;
			} else if (type === 'sell') {
				let msgBeforeSell = {
					success: true,
					item: item
				};
				events.emit('onBeforeSell', msgBeforeSell);
				if (!msgBeforeSell.success)
					return;
			}

			if (item.type === 'resource')
				events.emit('onGetResource', item.name, type === 'buy' ? 1 : -1);
			else {
				let bpt = $.extend(true, {}, item.bpt);
				bpt.id = objects.getNextId();
				events.emit('onGetObject', bpt);
			}

			events.emit('onGetResource', 'coins', type === 'buy' ? -item.buyPrice : item.price);

			this.buildShop(this.action);
		},

		getList: function (type) {
			let items = Object.entries(vendorItems)
				.filter(([k, v]) => {
					return (
						v.townLevel <= map.loaded &&
						(
							!v.bpt || 
							type === 'buy'
						)
					);
				})
				.map(([k, v]) => {
					return {
						name: k,
						...v,
						buyPrice: v.price * this.markup
					};
				});

			return items;
		},

		events: {
			onVendorArrives: function (obj) {
				sound.play('talk');
				this.vendor = obj;

				this.find('.title').html('The merchant has arrived');
				this.show();	
			},

			onVendorLeaves: function () {
				this.hide();
				this.hideShop();
			},

			onDismiss: function () {
				sound.play('tick');
				this.vendor.ai.completeAction();
			}
		}
	};
});
