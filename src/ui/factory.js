define([
	'ui/uiBase',
	'js/system/events'
], function (
	uiBase,
	events
) {
	return {
		uis: [],
		root: '',

		init: function (root) {
			if (root)
				this.root = root + '/';

			events.on('onEnterGame', this.onEnterGame.bind(this));
			events.on('onUiKeyDown', this.onUiKeyDown.bind(this));
			events.on('onResize', this.onResize.bind(this));
		},

		onEnterGame: function () {
			events.clearQueue();

			[
				'hud',
				'info',
				'vendor|.bottom-left',
				'builder|.bottom-left',
				'general|.bottom-left'
			].forEach(function (u) {
				let split = u.split('|');
				this.build(split[0], {
					container: split[1]
				});
			}, this);
		},

		build: async function (type, options) {
			return new Promise(res => {
				let className = 'ui' + type[0].toUpperCase() + type.substr(1);
				let el = $('.' + className);
				if (el.length > 0)
					return;

				require([this.root + 'ui/templates/' + type + '/' + type], this.onGetTemplate.bind(this, type, options, res));
			});
		},

		onGetTemplate: function (type, options, resolver, template) {
			let ui = $.extend(true, {}, uiBase, template, options);
			ui.type = type;

			requestAnimationFrame(this.renderUi.bind(this, ui, resolver));
		},

		renderUi: function (ui, resolver) {
			ui.render();
			ui.el.data('ui', ui);

			this.uis.push(ui);

			resolver(ui);
		},

		onResize: function () {
			this.uis.forEach(function (ui) {
				if (ui.centered)
					ui.center();
				else if ((ui.centeredX) || (ui.centeredY))
					ui.center(ui.centeredX, ui.centeredY);
			}, this);
		},

		onUiKeyDown: function (keyEvent) {
			if (keyEvent.key === 'esc') {
				this.uis.forEach(function (u) {
					if (!u.modal || !u.shown)
						return;

					keyEvent.consumed = true;
					u.hide();
				});
				$('.uiOverlay').hide();
				events.emit('onHideContextMenu');
			} else if (['o', 'j', 'h', 'i'].indexOf(keyEvent.key) > -1)
				$('.uiOverlay').hide();
		},

		destroy: function (type) {
			if (type) {
				let ui = this.uis.spliceFirstWhere(u => u.type === type);
				ui.destroy();
			} else {
				this.uis.forEach(u => u.destroy());
				this.uis = [];
			}
		},

		preload: function () {
			require([
				'death',
				'dialogue',
				'equipment',
				'events',
				'hud',
				'inventory',
				'overlay',
				'passives',
				'quests',
				'reputation',
				'smithing',
				'stash'
			].map(m => 'ui/templates/' + m + '/' + m), this.afterPreload.bind(this));
		},

		afterPreload: function () {
			this.build('characters', {});
		},

		update: function () {
			let uis = this.uis;
			let uLen = uis.length;
			for (let i = 0; i < uLen; i++) {
				let u = uis[i];
				if (u.update)
					u.update();
			}
		}
	};
});
